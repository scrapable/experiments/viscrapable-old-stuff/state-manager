(async () => {
    let recordingState = "manual"

    const getRecordingState = () => recordingState
    const setRecordingState = async newState => {
        const oldState = recordingState
        recordingState = newState
        dispatchStateEvent("recordingStateChanged", {
            oldValue: oldState,
            newValue: recordingState
        })
        if(oldState !== newState) {
            if(newState === "playback") {
                await executeEvent({ op: "reset" })
                for (const event of eventQueue) {
                    if(recordingState !== "playback") {
                        console.log("aborting playback")
                        break
                    }
                    await executeEvent(event)
                }
                await setRecordingState("manual")
            }
            else if(newState === "recording") {
                lastInstructionTimestamp = null
                clearEventQueue()
            }
        }
    }


    let eventQueue = []
    let lastInstructionTimestamp

    const dispatchEvent = async event => {
        if(recordingState === "playback")
            return
        const isRecording = recordingState === "recording"
        event.attachMetadata = isRecording
        await executeEvent(event)
        delete event.attachMetadata
        if(isRecording) {
            if(event.delayBefore) {
                lastInstructionTimestamp += instruction.delayBefore
            }
            else {
                const now = Date.now()
                event.delayBefore = lastInstructionTimestamp ? (now - lastInstructionTimestamp) : 0
                lastInstructionTimestamp = now
            }
            mergeEventIntoQueue(event)
        }
    }
    const executeEvent = async event =>
        await dispatchAsyncStateEvent("eventExecuted", event)

    const mergeEventIntoQueue = event => {
        console.log(event)
        const focused = event.metadata.focusedElementInfo

        let lastIndex
        let lastEvent
        const refreshLast = () => {
            lastIndex = eventQueue.length - 1
            lastEvent = eventQueue[lastIndex]
        }
        refreshLast()

        const dispatchChanged = () =>
            dispatchStateEvent("eventChangedInQueue", {
                index: lastIndex,
                newValue: lastEvent
            })

        if(
            (event.op === "keyUp" || event.op === "keyDown") &&
            !(event.args.code === "Enter" || event.args.code === "Tab") &&
            focused && focused.tagName === "INPUT" &&
            (focused.type === null || focused.type === "text" || focused.type === "password")
        ) {
            const text = focused.value
            if(lastEvent && lastEvent.op === "type" && lastEvent.args.selector === event.metadata.focusedSelector) {
                lastEvent.args.text = text
                dispatchChanged()
            } else {
                pushEventIntoQueue({
                    op: "type",
                    args: {
                        text: focused.value,
                        selector: event.metadata.focusedSelector
                    },
                    delayBefore: event.delayBefore,
                    metadata: event.metadata
                })
            }
        } else if(
            focused && focused.tagName === "INPUT" && focused.type === "checkbox" &&
            (event.op === "keyUp" && event.args.code === " " || event.op === "mouseUp")
        ) {
            if(lastEvent.metadata.focusedSelector === event.metadata.focusedSelector && (
                event.op === "keyUp" && lastEvent.op === "keyDown" && lastEvent.args.code === " " ||
                event.op === "mouseUp" && lastEvent.op === "mouseDown"
            )) {
                removeEventFromQueue(lastIndex)
                refreshLast()
            }
            if(lastEvent.op === "check") {
                lastEvent.args.checked = focused.checked
                dispatchChanged()
            } else {
                pushEventIntoQueue({
                    op: "check",
                    args: {
                        checked: focused.checked,
                        selector: event.metadata.focusedSelector
                    },
                    delayBefore: event.delayBefore,
                    metadata: event.metadata
                })
            }
        } else {
            pushEventIntoQueue(event)
        }
    }

    const pushEventIntoQueue = event => {
        eventQueue.push(event)
        dispatchStateEvent("eventAppendedToQueue", { event })
    }

    const removeEventFromQueue = index => {
        const removed = eventQueue[index]
        eventQueue.splice(index)
        dispatchStateEvent("eventRemovedFromQueue", {
            index,
            event: removed
        })
    }

    const getEventQueue = () => eventQueue
    const setEventQueue = newQueue => {
        const oldValue = eventQueue
        eventQueue = newQueue
        dispatchStateEvent("queueSwapped", {
            oldValue,
            newValue: eventQueue
        })
    }
    const clearEventQueue = () => setEventQueue([])


    const eventListeners = {
        recordingStateChanged: [],
        eventAppendedToQueue: [],
        eventChangedInQueue: [],
        eventRemovedFromQueue: [],
        eventExecuted: [],
        queueSwapped: []
    }

    const addEventListener = (eventName, listener) =>
        eventListeners[eventName].push(listener)
    const removeEventListener = listenerId => {
        eventListeners[eventName][listenerId] = null
    }

    const dispatchStateEvent = (eventName, event) =>
        allListeners(eventName).forEach(l => l(event))
    const dispatchAsyncStateEvent = async (eventName, event) =>
        await Promise.all(
            allListeners(eventName).map(l => l(event))
        )
    const allListeners = eventName =>
        eventListeners[eventName].filter(l => l !== null)


    module.exports = {
        getRecordingState,
        setRecordingState,
        dispatchEvent,
        getEventQueue,
        setEventQueue,
        clearEventQueue,
        addEventListener,
        removeEventListener
    }
})()
